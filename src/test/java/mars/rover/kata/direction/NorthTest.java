package mars.rover.kata.direction;

import java.awt.geom.Point2D;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class NorthTest {

    private North north = new North();

    private Point2D.Double limit;

    @Before
    public void before() {
        limit = new Point2D.Double(10, 20);
    }

    @Test
    public void whenMoveBackwardThenShouldMoveOneStepDown() {
        final double positionX = 1;
        final double positionY = 2;
        final Point2D.Double point = new Point2D.Double(positionX, positionY);
        final Point2D newPoint = north.moveBackward(point, limit);
        assertEquals(new Point2D.Double(positionX, positionY - 1), newPoint);
    }

    @Test
    public void whenMoveBackwardAndGoesOutOfLimitThenShouldMoveToTheVerticalLimit() {
        final double positionX = 1;
        final double positionY = 0;
        final Point2D.Double point = new Point2D.Double(positionX, positionY);
        final Point2D newPoint = north.moveBackward(point, limit);
        assertEquals(new Point2D.Double(positionX, limit.getY()), newPoint);
    }

    @Test
    public void whenMoveForwardThenShouldMoveOneStepUp() {
        final double positionX = 1;
        final double positionY = 2;
        final Point2D.Double point = new Point2D.Double(positionX, positionY);
        final Point2D newPoint = north.moveForward(point, limit);
        assertEquals(new Point2D.Double(positionX, positionY + 1), newPoint);
    }

    @Test
    public void whenMoveForwardAndGoesOutOfLimitThenShouldMoveToTheOtherSideLimit() {
        final double positionX = 1;
        final double positionY = 20;
        final Point2D.Double point = new Point2D.Double(positionX, positionY);
        final Point2D newPoint = north.moveForward(point, limit);
        assertEquals(new Point2D.Double(positionX, 0), newPoint);
    }

    @Test
    public void whenTurnLeftThenShouldMovesOnWestDirection() {
        final Direction direction = north.turnLeft();
        assertTrue(direction instanceof West);
    }

    @Test
    public void whenTurnRightThenShouldMovesOnEastDirection() {
        final Direction direction = north.turnRight();
        assertTrue(direction instanceof East);
    }
}
