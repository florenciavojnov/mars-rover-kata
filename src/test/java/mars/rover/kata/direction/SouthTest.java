package mars.rover.kata.direction;

import java.awt.geom.Point2D;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SouthTest {

    private South south = new South();

    private Point2D.Double limit;

    @Before
    public void before() {
        limit = new Point2D.Double(10, 20);
    }

    @Test
    public void whenMoveBackwardThenShouldMoveOneStepUp() {
        final double positionX = 1;
        final double positionY = 2;
        final Point2D.Double point = new Point2D.Double(positionX, positionY);
        final Point2D newPoint = south.moveBackward(point, limit);
        assertEquals(new Point2D.Double(positionX, positionY + 1), newPoint);
    }

    @Test
    public void whenMoveBackwardAndGoesOutOfLimitThenShouldMoveToTheVerticalLimit() {
        final double positionX = 1;
        final double positionY = 20;
        final Point2D.Double point = new Point2D.Double(positionX, positionY);
        final Point2D newPoint = south.moveBackward(point, limit);
        assertEquals(new Point2D.Double(positionX, 0), newPoint);
    }

    @Test
    public void whenMoveForwardThenShouldMoveOneStepDown() {
        final double positionX = 1;
        final double positionY = 2;
        final Point2D.Double point = new Point2D.Double(positionX, positionY);
        final Point2D newPoint = south.moveForward(point, limit);
        assertEquals(new Point2D.Double(positionX, positionY - 1), newPoint);
    }

    @Test
    public void whenMoveForwardAndGoesOutOfLimitThenShouldMoveToTheOtherSideLimit() {
        final double positionX = 1;
        final double positionY = 0;
        final Point2D.Double point = new Point2D.Double(positionX, positionY);
        final Point2D newPoint = south.moveForward(point, limit);
        assertEquals(new Point2D.Double(positionX, limit.getY()), newPoint);
    }

    @Test
    public void whenTurnLeftThenShouldMovesOnEastDirection() {
        final Direction direction = south.turnLeft();
        assertTrue(direction instanceof East);
    }

    @Test
    public void whenTurnRightThenShouldMovesOnWestDirection() {
        final Direction direction = south.turnRight();
        assertTrue(direction instanceof West);
    }
}
