package mars.rover.kata.direction;

import java.awt.geom.Point2D;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class WestTest {

    private West west = new West();

    private Point2D.Double limit;

    @Before
    public void before() {
        limit = new Point2D.Double(10, 20);
    }

    @Test
    public void whenMoveBackwardThenShouldMoveOneStepToTheRight() {
        final double positionX = 1;
        final double positionY = 2;
        final Point2D.Double point = new Point2D.Double(positionX, positionY);
        final Point2D newPoint = west.moveBackward(point, limit);
        assertEquals(new Point2D.Double(positionX + 1, positionY), newPoint);
    }

    @Test
    public void whenMoveBackwardAndGoesOutOfLimitThenShouldMoveToTheHorizontalLimit() {
        final double positionX = 10;
        final double positionY = 2;
        final Point2D.Double point = new Point2D.Double(positionX, positionY);
        final Point2D newPoint = west.moveBackward(point, limit);
        assertEquals(new Point2D.Double(0, positionY), newPoint);
    }

    @Test
    public void whenMoveForwardThenShouldMoveOneStepToTheLeft() {
        final double positionX = 1;
        final double positionY = 2;
        final Point2D.Double point = new Point2D.Double(positionX, positionY);
        final Point2D newPoint = west.moveForward(point, limit);
        assertEquals(new Point2D.Double(positionX - 1, positionY), newPoint);
    }

    @Test
    public void whenMoveForwardAndGoesOutOfLimitThenShouldMoveToTheOtherSideLimit() {
        final double positionX = 0;
        final double positionY = 2;
        final Point2D.Double point = new Point2D.Double(positionX, positionY);
        final Point2D newPoint = west.moveForward(point, limit);
        assertEquals(new Point2D.Double(limit.getX(), positionY), newPoint);
    }

    @Test
    public void whenTurnLeftThenShouldMovesOnSouthDirection() {
        final Direction direction = west.turnLeft();
        assertTrue(direction instanceof South);
    }

    @Test
    public void whenTurnRightThenShouldMovesOnNorthDirection() {
        final Direction direction = west.turnRight();
        assertTrue(direction instanceof North);
    }

}
