package mars.rover.kata;

import com.google.common.collect.ImmutableMap;
import java.awt.geom.Point2D;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import mars.rover.kata.direction.East;
import mars.rover.kata.direction.North;
import mars.rover.kata.direction.South;
import mars.rover.kata.direction.West;
import mars.rover.kata.command.Backward;
import mars.rover.kata.command.Forward;
import mars.rover.kata.command.Command;
import mars.rover.kata.command.TurnLeft;
import mars.rover.kata.command.TurnRight;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RoverTest {
    private Planet mars;
    private Map<String, Command> movements =
        ImmutableMap.of("B", new Backward(), "F", new Forward(), "L", new TurnLeft(), "R", new TurnRight());

    @Before
    public void before() {
        mars = new Planet(20, 10, movements);
    }

    @Test
    public void whenCreatesNewRoverWithValidPositionThenCreatesIt() {
        final double positionX = 1;
        final double positionY = 2;
        final Rover rover = new Rover(positionX, positionY, new North(), mars);

        final Point2D.Double expectedPosition = new Point2D.Double(positionX, positionY);
        assertEquals(expectedPosition, rover.getPosition());
        assertTrue(rover.getDirection() instanceof North);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenCreatesNewRoverInAPositionOutOfThePlanetThenFails() {
        new Rover(mars.getPlanetSize().getX() + 1, 0, new North(), mars);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenCreatesNewRoverWithANegativeXPointThenFails() {
        new Rover(-1, 2, new North(), mars);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenCreatesNewRoverWithANegativeYPointThenFails() {
        new Rover(1, -2, new North(), mars);
    }

    @Test
    public void whenRoverMovesForwardNorthThenMovesOnePositionUp() {
        final Rover rover = new Rover(1, 2, new North(), mars);

        rover.executeCommand(Arrays.asList("F"));
        assertEquals(new Point2D.Double(1, 3), rover.getPosition());
        assertTrue(rover.getDirection() instanceof North);
    }

    @Test
    public void whenRoverMovesForwardNorthAndExceedsPlanetSizeThenFollowsAtBottom() {
        final Rover rover = new Rover(1, mars.getPlanetSize().getY(), new North(), mars);

        rover.executeCommand(Arrays.asList("F"));
        assertEquals(new Point2D.Double(1, 0), rover.getPosition());
        assertTrue(rover.getDirection() instanceof North);
    }

    @Test
    public void whenRoverMovesForwardSouthThenMovesOnePositionDown() {
        final Rover rover = new Rover(1, 2, new South(), mars);

        rover.executeCommand(Arrays.asList("F"));
        assertEquals(new Point2D.Double(1, 1), rover.getPosition());
        assertTrue(rover.getDirection() instanceof South);
    }

    @Test
    public void whenRoverMovesForwardSouthAndExceedsPlanetSizeThenFollowsAtTop() {
        final Rover rover = new Rover(1, 0, new South(), mars);

        rover.executeCommand(Arrays.asList("F"));
        assertEquals(new Point2D.Double(1, mars.getPlanetSize().getY()), rover.getPosition());
        assertTrue(rover.getDirection() instanceof South);
    }

    @Test
    public void whenRoverMovesForwardEastThenMovesOnePositionToRight() {
        final Rover rover = new Rover(1, 2, new East(), mars);

        rover.executeCommand(Arrays.asList("F"));
        assertEquals(new Point2D.Double(2, 2), rover.getPosition());
        assertTrue(rover.getDirection() instanceof East);
    }

    @Test
    public void whenRoverMovesForwardEastAndExceedsPlanetSizeThenFollowsAtTheOtherSide() {
        final Rover rover = new Rover(mars.getPlanetSize().getX(), 2, new East(), mars);

        rover.executeCommand(Arrays.asList("F"));
        assertEquals(new Point2D.Double(0, 2), rover.getPosition());
        assertTrue(rover.getDirection() instanceof East);
    }

    @Test
    public void whenRoverMovesForwardWestThenMovesOnePositionToLeft() {
        final Rover rover = new Rover(1, 2, new West(), mars);

        rover.executeCommand(Arrays.asList("F"));
        assertEquals(new Point2D.Double(0, 2), rover.getPosition());
        assertTrue(rover.getDirection() instanceof West);
    }

    @Test
    public void whenRoverMovesForwardWestAndExceedsPlanetSizeThenFollowsAtTheOtherSide() {
        final Rover rover = new Rover(0, 2, new West(), mars);

        rover.executeCommand(Arrays.asList("F"));
        assertEquals(new Point2D.Double(mars.getPlanetSize().getX(), 2), rover.getPosition());
        assertTrue(rover.getDirection() instanceof West);
    }

    @Test
    public void whenRoverMovesBackwardNorthThenMovesOnePositionDown() {
        final Rover rover = new Rover(0, 2, new North(), mars);

        rover.executeCommand(Arrays.asList("B"));
        assertEquals(new Point2D.Double(0, 1), rover.getPosition());
        assertTrue(rover.getDirection() instanceof North);
    }

    @Test
    public void whenRoverMovesBackwardNorthAndExceedsPlanetSizeThenFollowsAtTop() {
        final Rover rover = new Rover(0, 0, new North(), mars);

        rover.executeCommand(Arrays.asList("B"));
        assertEquals(new Point2D.Double(0, mars.getPlanetSize().getY()), rover.getPosition());
        assertTrue(rover.getDirection() instanceof North);
    }

    @Test
    public void whenRoverMovesBackwardSouthThenMovesOnePositionUp() {
        final Rover rover = new Rover(1, 2, new South(), mars);

        rover.executeCommand(Arrays.asList("B"));
        assertEquals(new Point2D.Double(1, 3), rover.getPosition());
        assertTrue(rover.getDirection() instanceof South);
    }

    @Test
    public void whenRoverMovesBackwardSouthAndExceedsPlanetSizeThenFollowsAtBottom() {
        final Rover rover = new Rover(1, mars.getPlanetSize().getY(), new South(), mars);

        rover.executeCommand(Arrays.asList("B"));
        assertEquals(new Point2D.Double(1, 0), rover.getPosition());
        assertTrue(rover.getDirection() instanceof South);
    }

    @Test
    public void whenRoverMovesBackwardEastThenMovesOnePositionToLeft() {
        final Rover rover = new Rover(1, 2, new East(), mars);

        rover.executeCommand(Arrays.asList("B"));
        assertEquals(new Point2D.Double(0, 2), rover.getPosition());
        assertTrue(rover.getDirection() instanceof East);
    }

    @Test
    public void whenRoverMovesBackwardEastAndExceedsPlanetSizeThenFollowsAtTheOtherSide() {
        final Rover rover = new Rover(0, 2, new East(), mars);

        rover.executeCommand(Arrays.asList("B"));
        assertEquals(new Point2D.Double(mars.getPlanetSize().getX(), 2), rover.getPosition());
        assertTrue(rover.getDirection() instanceof East);
    }

    @Test
    public void whenRoverMovesBackwardWestThenMovesOnePositionToRight() {
        final Rover rover = new Rover(1, 2, new West(), mars);

        rover.executeCommand(Arrays.asList("B"));
        assertEquals(new Point2D.Double(2, 2), rover.getPosition());
        assertTrue(rover.getDirection() instanceof West);
    }

    @Test
    public void whenRoverMovesBackwardWestAndExceedsPlanetSizeThenFollowsAtTheOtherSide() {
        final Rover rover = new Rover(mars.getPlanetSize().getX(), 2, new West(), mars);

        rover.executeCommand(Arrays.asList("B"));
        assertEquals(new Point2D.Double(0, 2), rover.getPosition());
        assertTrue(rover.getDirection() instanceof West);
    }

    @Test
    public void whenRoverTurnLeftOnNorthThenNewDirectionIsWest() {
        final Rover rover = new Rover(0, 2, new North(), mars);

        rover.executeCommand(Arrays.asList("L"));
        assertTrue(rover.getDirection() instanceof West);
    }

    @Test
    public void whenRoverTurnLeftOnWestThenNewDirectionIsSouth() {
        final Rover rover = new Rover(0, 2, new West(), mars);

        rover.executeCommand(Arrays.asList("L"));
        assertTrue(rover.getDirection() instanceof South);
    }

    @Test
    public void whenRoverTurnLeftOnSouthThenNewDirectionIsEast() {
        final Rover rover = new Rover(0, 2, new South(), mars);

        rover.executeCommand(Arrays.asList("L"));
        assertTrue(rover.getDirection() instanceof East);
    }

    @Test
    public void whenRoverTurnLeftOnEastThenNewDirectionIsNorth() {
        final Rover rover = new Rover(0, 2, new East(), mars);

        rover.executeCommand(Arrays.asList("L"));
        assertTrue(rover.getDirection() instanceof North);
    }

    @Test
    public void whenRoverTurnRightOnNorthThenNewDirectionIsEast() {
        final Rover rover = new Rover(0, 2, new North(), mars);

        rover.executeCommand(Arrays.asList("R"));
        assertTrue(rover.getDirection() instanceof East);
    }

    @Test
    public void whenRoverTurnRightOnWestThenNewDirectionIsNorth() {
        final Rover rover = new Rover(0, 2, new West(), mars);

        rover.executeCommand(Arrays.asList("R"));
        assertTrue(rover.getDirection() instanceof North);
    }

    @Test
    public void whenRoverTurnRightOnSouthThenNewDirectionIsWest() {
        final Rover rover = new Rover(0, 2, new South(), mars);

        rover.executeCommand(Arrays.asList("R"));
        assertTrue(rover.getDirection() instanceof West);
    }

    @Test
    public void whenRoverTurnRigthOnEastThenNewDirectionIsSouth() {
        final Rover rover = new Rover(0, 2, new East(), mars);

        rover.executeCommand(Arrays.asList("R"));
        assertTrue(rover.getDirection() instanceof South);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenRoverInitialPositionHasAnObstacleThenFails() {
        final List<Obstacle> obstacles = Arrays.asList(new Obstacle(2, 2));
        final Planet planet = new Planet(20, 10, movements, obstacles);
        new Rover(2, 2, new East(), planet);
    }

    @Test(expected = RuntimeException.class)
    public void whenRoverMovesAndThereIsAnObstacleThenKeepPosition() {
        final List<Obstacle> obstacles = Arrays.asList(new Obstacle(2, 2));
        final Planet planet = new Planet(20, 10, movements, obstacles);
        final Rover rover = new Rover(1, 2, new East(), planet);

        rover.executeCommand(Arrays.asList("F"));
        assertEquals(new Point2D.Double(1, 2), rover.getPosition());
    }

    @Test
    public void whenRoverReceivesMoreThanOneCommandThenExecutesThemAll() {
        final Planet planet = new Planet(20, 10, movements);
        final Rover rover = new Rover(1, 2, new East(), planet);

        rover.executeCommand(Arrays.asList("F", "B", "L"));
        assertEquals(new Point2D.Double(1, 2), rover.getPosition());
        assertTrue(rover.getDirection() instanceof North);
    }

    @Test
    public void whenRoverReceivesMoreThanOneCommandAndDoesNotCollideAnyObstacleThenExecutesThemAll() {
        final List<Obstacle> obstacles = Arrays.asList(new Obstacle(4, 2));
        final Planet planet = new Planet(20, 10, movements, obstacles);
        final Rover rover = new Rover(1, 2, new East(), planet);

        rover.executeCommand(Arrays.asList("F", "F", "B", "F", "L"));
        assertEquals(new Point2D.Double(3, 2), rover.getPosition());
        assertTrue(rover.getDirection() instanceof North);
    }

    @Test(expected = RuntimeException.class)
    public void whenRoverReceivesMoreThanOneCommandAndThereIsAnObstacleThenKeepLastValidPositionAndDoNotExecuteFollowingCommands() {
        final List<Obstacle> obstacles = Arrays.asList(new Obstacle(4, 2));
        final Planet planet = new Planet(20, 10, movements, obstacles);
        final Rover rover = new Rover(1, 2, new East(), planet);

        rover.executeCommand(Arrays.asList("F", "F", "F", "F", "L"));
        assertEquals(new Point2D.Double(3, 2), rover.getPosition());
        assertTrue(rover.getDirection() instanceof East);
    }


    @Test(expected = RuntimeException.class)
    public void whenRoverReceivesMoreThanOneCommandAndOneIsInvalidThenFails() {
        final List<Obstacle> obstacles = Arrays.asList(new Obstacle(7, 2));
        final Planet planet = new Planet(20, 10, movements, obstacles);
        final Rover rover = new Rover(1, 2, new East(), planet);

        rover.executeCommand(Arrays.asList("F", "F", "F", "I", "L"));
        assertEquals(new Point2D.Double(4, 2), rover.getPosition());
        assertTrue(rover.getDirection() instanceof North);
    }
}