package mars.rover.kata;

import java.awt.geom.Point2D;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ObstacleTest {

    @Test
    public void whenCreatesObstacleWithValidPositionThenCreatesIt() {
        final double sizeX = 20;
        final double sizeY = 20;

        final Obstacle obstacle = new Obstacle(sizeX, sizeY);

        final Point2D.Double expectedPlanetSize = new Point2D.Double(sizeX, sizeY);
        assertEquals(expectedPlanetSize, obstacle.getPosition());
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenObstacleHorizontalPointIsNegativeThenFails() {
        new Obstacle(-1, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenObstacleVerticalPointIsNegativeThenFails() {
        new Obstacle(1, -1);
    }
}
