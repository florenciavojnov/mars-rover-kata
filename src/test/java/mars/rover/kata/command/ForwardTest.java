package mars.rover.kata.command;

import mars.rover.kata.Rover;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ForwardTest {

    private Forward forward = new Forward();

    @Test
    public void whenExecuteThenRoverShouldCallForwardMovement() {
        final Rover rover = mock(Rover.class);
        forward.execute(rover);
        verify(rover).moveForward();
    }
}
