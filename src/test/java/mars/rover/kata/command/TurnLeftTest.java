package mars.rover.kata.command;

import mars.rover.kata.Rover;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TurnLeftTest {

    private TurnLeft turnLeft = new TurnLeft();

    @Test
    public void whenExecuteThenRoverShouldCallTurnLeftMovement() {
        final Rover rover = mock(Rover.class);
        turnLeft.execute(rover);
        verify(rover).turnLeft();
    }
}
