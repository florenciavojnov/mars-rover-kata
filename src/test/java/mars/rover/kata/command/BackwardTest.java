package mars.rover.kata.command;

import mars.rover.kata.Rover;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class BackwardTest {

    private Backward backward = new Backward();

    @Test
    public void whenExecuteThenRoverShouldCallBackwardMovement() {
        final Rover rover = mock(Rover.class);
        backward.execute(rover);
        verify(rover).moveBackward();
    }
}
