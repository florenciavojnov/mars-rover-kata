package mars.rover.kata.command;

import mars.rover.kata.Rover;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TurnRightTest {

    private TurnRight turnRight = new TurnRight();

    @Test
    public void whenExecuteThenRoverShouldCallTurnRightMovement() {
        final Rover rover = mock(Rover.class);
        turnRight.execute(rover);
        verify(rover).turnRight();
    }
}
