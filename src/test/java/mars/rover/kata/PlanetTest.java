package mars.rover.kata;

import com.google.common.collect.ImmutableMap;
import java.awt.geom.Point2D;
import java.util.Arrays;
import java.util.Map;
import mars.rover.kata.command.Backward;
import mars.rover.kata.command.Forward;
import mars.rover.kata.command.Command;
import mars.rover.kata.command.TurnLeft;
import mars.rover.kata.command.TurnRight;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PlanetTest {

    private Map<String, Command> movements =
        ImmutableMap.of("B", new Backward(), "F", new Forward(), "L", new TurnLeft(), "R", new TurnRight());

    @Test
    public void whenCreatesNewPlanetWithValidSizeThenCreatesIt() {
        final double sizeX = 20;
        final double sizeY = 20;

        final Planet planet = new Planet(sizeX, sizeY, movements);

        final Point2D.Double expectedPlanetSize = new Point2D.Double(sizeX, sizeY);
        assertEquals(expectedPlanetSize, planet.getPlanetSize());
        assertTrue(planet.getObstacles().isEmpty());
    }

    @Test
    public void whenCreatesNewPlanetWithValidSizeAndObstaclesThenCreatesIt() {
        final double sizeX = 20;
        final double sizeY = 20;

        final Planet planet = new Planet(sizeX, sizeY, movements, Arrays.asList(new Obstacle(1, 1)));

        final Point2D.Double expectedPlanetSize = new Point2D.Double(sizeX, sizeY);
        assertEquals(expectedPlanetSize, planet.getPlanetSize());
        assertFalse(planet.getObstacles().isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenPlanetSizeIsZeroThenFails() {
        new Planet(0, 0, movements);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenPlanetHorizontalMapSizeIsNegativeThenFails() {
        new Planet(-1, 1, movements);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenPlanetVerticalMapSizeIsNegativeThenFails() {
        new Planet(1, -1, movements, Arrays.asList());
    }
}
