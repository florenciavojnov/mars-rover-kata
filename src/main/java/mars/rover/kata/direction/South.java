package mars.rover.kata.direction;

import java.awt.geom.Point2D;

public class South implements Direction {

    @Override
    public Point2D moveBackward(final Point2D position, final Point2D limit) {
        if (position.getY() + 1 > limit.getY()) {
            return new Point2D.Double(position.getX(), 0);
        }
        return new Point2D.Double(position.getX(), position.getY() + 1);
    }

    @Override
    public Point2D moveForward(final Point2D position, final Point2D limit) {
        if (position.getY() - 1 < 0) {
            return new Point2D.Double(position.getX(), limit.getY());
        }
        return new Point2D.Double(position.getX(), position.getY() - 1);
    }

    @Override
    public Direction turnLeft() {
        return new East();
    }

    @Override
    public Direction turnRight() {
        return new West();
    }
}