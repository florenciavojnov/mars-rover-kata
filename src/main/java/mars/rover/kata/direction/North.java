package mars.rover.kata.direction;

import java.awt.geom.Point2D;

public class North implements Direction {

    @Override
    public Point2D moveBackward(final Point2D position, final Point2D limit) {
        if (position.getY() > 0) {
            return new Point2D.Double(position.getX(), position.getY() - 1);
        }
        return new Point2D.Double(position.getX(), limit.getY());
    }

    @Override
    public Point2D moveForward(final Point2D position, final Point2D limit) {
        if (position.getY() < limit.getY()) {
            return new Point2D.Double(position.getX(), position.getY() + 1);
        }
        return new Point2D.Double(position.getX(), 0);
    }

    @Override
    public Direction turnLeft() {
        return new West();
    }

    @Override
    public Direction turnRight() {
        return new East();
    }
}