package mars.rover.kata.direction;

import java.awt.geom.Point2D;

public class West implements Direction {

    @Override
    public Point2D moveBackward(final Point2D position, final Point2D limit) {
        if (position.getX() + 1 > limit.getX()) {
            return new Point2D.Double(0, position.getY());
        }
        return new Point2D.Double(position.getX() + 1, position.getY());
    }

    @Override
    public Point2D moveForward(final Point2D position, final Point2D limit) {
        if (position.getX() - 1 < 0) {
            return new Point2D.Double(limit.getX(), position.getY());
        }
        return new Point2D.Double(position.getX() - 1, position.getY());
    }

    @Override
    public Direction turnLeft() {
        return new South();
    }

    @Override
    public Direction turnRight() {
        return new North();
    }
}