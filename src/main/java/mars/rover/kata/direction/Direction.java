package mars.rover.kata.direction;

import java.awt.geom.Point2D;

public interface Direction {

    Point2D moveBackward(final Point2D position, final Point2D limit);

    Point2D moveForward(Point2D position, final Point2D limit);

    Direction turnLeft();

    Direction turnRight();
}
