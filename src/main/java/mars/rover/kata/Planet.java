package mars.rover.kata;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import mars.rover.kata.command.Command;

public class Planet {

    private final Point2D planetSize;
    private final Map<String, Command> movements;
    private List<Obstacle> obstacles = new ArrayList<>();

    public Planet(final double sizeX, final double sizeY, Map<String, Command> movements) {
        if (sizeX <= 0 || sizeY <= 0) {
            throw new IllegalArgumentException("Planet size can not be zero or less");
        }
        this.planetSize = new Point2D.Double(sizeX, sizeY);
        this.movements = movements;
    }

    public Planet(final double sizeX, final double sizeY, Map<String, Command> movements, final List<Obstacle> obstacles) {
        if (sizeX <= 0 || sizeY <= 0) {
            throw new IllegalArgumentException("Planet size can not be zero or less");
        }
        this.planetSize = new Point2D.Double(sizeX, sizeY);
        this.movements = movements;
        this.obstacles = obstacles;
    }

    public Point2D getPlanetSize() {
        return planetSize;
    }

    public Map<String, Command> getMovements() {
        return movements;
    }

    public List<Obstacle> getObstacles() {
        return obstacles;
    }

    public boolean hasPositionAnObstacle(final Point2D position) {
        return getObstacles().stream().anyMatch(obstacle -> obstacle.getPosition().equals(position));
    }
}