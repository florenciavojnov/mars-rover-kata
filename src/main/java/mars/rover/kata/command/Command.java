package mars.rover.kata.command;

import mars.rover.kata.Rover;

public interface Command {

    void execute(final Rover rover);
}
