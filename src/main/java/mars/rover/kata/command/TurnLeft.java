package mars.rover.kata.command;

import mars.rover.kata.Rover;

public class TurnLeft implements Command {

    @Override
    public void execute(final Rover rover) {
        rover.turnLeft();
    }
}
