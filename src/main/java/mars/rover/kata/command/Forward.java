package mars.rover.kata.command;

import mars.rover.kata.Rover;

public class Forward implements Command {

    @Override
    public void execute(final Rover rover) {
        rover.moveForward();
    }
}
