package mars.rover.kata;

import java.awt.geom.Point2D;

public class Obstacle {

    private Point2D position;

    public Obstacle(final double positionX, final double positionY) {
        if (positionX < 0 || positionY < 0) {
            throw new IllegalArgumentException("Invalid obstacle position");
        }
        this.position = new Point2D.Double(positionX, positionY);
    }

    public Point2D getPosition() {
        return position;
    }
}
