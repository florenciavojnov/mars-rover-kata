package mars.rover.kata;

import java.awt.geom.Point2D;
import java.util.List;
import mars.rover.kata.direction.Direction;
import mars.rover.kata.command.Command;

public class Rover {

    private Point2D position;
    private Direction direction;
    private final Planet planet;

    public Rover(final double positionX, final double positionY, final Direction direction, final Planet planet) {
        final Point2D.Double position = new Point2D.Double(positionX, positionY);
        if (positionX > planet.getPlanetSize().getX() || positionX < 0 ||
            positionY > planet.getPlanetSize().getY() || positionY < 0 ||
            planet.hasPositionAnObstacle(position)) {
            throw new IllegalArgumentException("Invalid rover initial position");
        }
        this.position = position;
        this.direction = direction;
        this.planet = planet;
    }

    public Point2D getPosition() {
        return position;
    }

    public void setPosition(final Point2D position) {
        this.position = position;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(final Direction direction) {
        this.direction = direction;
    }

    public Planet getPlanet() {
        return planet;
    }

    public void executeCommand(final List<String> movements) {
        movements.stream().forEach(movement -> {
            final Command command = planet.getMovements().get(movement);
            if (command != null) {
                command.execute(this);
            } else {
                throw new RuntimeException("Invalid command");
            }
        });
    }

    public void moveBackward() {
        final Point2D newPosition = this.getDirection().moveBackward(this.getPosition(), this.getPlanet().getPlanetSize());
        move(newPosition);
    }

    public void moveForward() {
        final Point2D newPosition = this.getDirection().moveForward(this.getPosition(), this.getPlanet().getPlanetSize());
        move(newPosition);
    }

    public void turnLeft() {
        final Direction newDirection = this.getDirection().turnLeft();
        rotate(newDirection);
    }

    public void turnRight() {
        final Direction newDirection = this.getDirection().turnRight();
        rotate(newDirection);
    }

    private void move(final Point2D newPosition) {
        if (planet.hasPositionAnObstacle(newPosition)) {
            throw new RuntimeException("There is an obstacle in this position.");
        } else {
            this.setPosition(newPosition);
        }
    }

    private void rotate(final Direction newDirection) {
        this.setDirection(newDirection);
    }
}