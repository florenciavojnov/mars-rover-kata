# Diseño propuesto:
- Para el manejo de la lógica de los `Movimientos` decidí implementar el patrón de diseño **Command** encapsulando de esta manera la lógica de cada acción en cada "comando/movimiento".
Dicha propuesta, además de presentar un código más limpio, evitando utilizar cláusulas if/else, simplifica agregar nuevos comandos siendo únicamente necesario implementar la interfaz correspondiente.

- En el caso de la `Dirección`, decidí utilizar el patrón **State** pudiendo así cambiar el comportamiento del vehículo de acuerdo a la dirección (que puede modificarse durante la ejecución).
Por otro lado, al igual que en los movimientos, este diseño permite un código más limpio, claro y mantenible evitando el uso de muchos statements if/else.
El uso del mencionado patrón permite encapsular la lógica en clases dedicadas, aplicando el principio de responsabilidad única y simplificando el agregado de nuevas direcciones, siendo únicamente necesario implementar la
interfaz correspondiente.

- La clase **Rover** representa el vehículo que se transporta en el Planeta. Conoce su posición, dirección y el planeta en el que se desplaza. Es responsable de moverse pero desliga la responsabilidad de como hacerlo a los movimientos y direcciones.